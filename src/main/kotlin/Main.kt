import com.nfeld.jsonpathkt.JsonPath
import com.nfeld.jsonpathkt.extension.read
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.cookies.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.serialization.json.*

suspend fun main(args: Array<String>) {
//    val client = HttpClient(CIO) {
//        install(HttpCookies) {
//            storage = ConstantCookiesStorage(Cookie(name = "SSID", value = "AnbPyFZr-68-Wo", domain = "www.youtube.com"))
//        }
//        install(Logging) {
//                logger = Logger.DEFAULT
//                level = LogLevel.HEADERS
//        }
//    }
//    val response: HttpResponse = client.post(url) {

    //    }
//    println(response.bodyAsText())

//   val asdf = response.bodyAsText()

    val stuff = JsonPath.parse(hugeJson)?.read<String>("$.profile.address.home")

    println(stuff)

}

private fun Any.println() {
    kotlin.io.println(this)
}

//fun JsonElement.search(text: String): JsonObject? {
//    when (this) {
//        is JsonArray -> {
//            forEach {
//                it.search(text)
//            }
//        }
//        is JsonObject -> {
//            this.
//        }
//        else -> TODO()
//    }
//
//    return if (keys.contains(text)) {
//        get(text) as JsonObject
//    } else {
//        keys.forEach {
//            when (val jsonItem = get(it)) {
//                is JsonArray -> {
//                    jsonItem.forEach {
//                        it.search(text)
//                    }
//                }
//                is JsonObject -> {
//                    jsonItem.search(text)
//                }
//                else -> {}
//            }
//
//        }
//        return null
//    }
//}

val hugeJson = """
    {
      "profile": {
         "name": "George",
         "address": {
             "home": "@ubuntu"
}
      }
    }
""".trimIndent()